
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCD
// Engineer: Robert Keenan Ciaran Nolan
//
// Create Date: 2.10.2017 14:00:00
// Design Name: State Machine Engine
// Module Name: engineStateMachine
// Project Name: State Machine in Verilog Lab
// Target Devices:
// Tool Versions:
// Description: Implementing the State Machine we had already designed in a previous lab
//              in Verilog and creating the relevant test bench
//Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////

module engineStateMachine(
    input clk,				//clock input
    input rst,				//asynchronous reset
    input button,			//starter button input
    input sensor,			//sensor input Engine off = 1 / Engine on = 0
    output reg enable,	    //enable output for the Engine
    output reg motor		//starter motor output
    );

    reg [1:0] currentState, nextState; //2 reg variables holding our current state and next state. 2 bits wide

    //Defining the 4 States that the state machine can be in

    localparam  [1:0]   OFF = 2'b00,       //Idle state where the engine is off
                        STARTING = 2'b01,  //Starting state where the engine is off but starter motor on
                        RUNNING = 2'b10,   //Engine Running state
                        STOPPING = 2'b11;  //Delay state for the engine as it is shutting down

    //Defining our register with asynchronous rest
    always @(posedge clk or posedge rst)
        begin
            if(rst)
                currentState <= OFF;
            else
                currentState <= nextState;
        end

    //Describing the next state logic for each
    always @(currentState, button, sensor)
        case(currentState)
                OFF:   if(button &&  sensor)
                            nextState = STARTING;
                        else
                            nextState = OFF;            //Accounting for faulty sensor
                STARTING:   if(!button && !sensor)      //Transition Condition
                                nextState = RUNNING;
                            else if(!button && sensor)
                                nextState = OFF;        //User takes finger off button before engine started
                            else
                                nextState = STARTING;
                RUNNING:    if(button)
                                nextState = STOPPING;   //Stopping Condition regardless of engine state
                            else if(!button && sensor)
                                nextState = OFF;        //Sudden failure of engine (stall)
                            else
                                nextState = RUNNING;    //Stay Running
                STOPPING:   if(!button && sensor)       //User releases button and engine off
                                nextState = OFF;
                            else
                                nextState = STOPPING;   //Stay in Stopping Delay state if
                                                        //above condition not satisifed
        endcase

    //Describe the output logic
    //We have built a Moore Machine (Output dependent only on current State)
    always @(currentState)
            case(currentState)
                OFF:   begin
                            enable = 1'b0;      //Fully off
                            motor = 1'b0;
                        end
                STARTING:   begin
                                enable = 1'b1;  //Need both outputs set to 1 to start engine
                                motor = 1'b1;
                            end
                RUNNING:    begin
                                enable = 1'b1;  //Enable = 1 to keep engine running
                                motor = 1'b0;   //Starter motor no longer needed
                            end
                STOPPING:   begin
                                enable = 1'b0;  //Turning the engine off
                                motor = 1'b0;
                            end
            endcase


endmodule
